<?php
/**
 * Created by PhpStorm.
 * User: mohamed
 * Date: 15/07/2017
 * Time: 23:14
 */

namespace Drupal\workbench_group\Plugin\GroupContentEnabler;


use Drupal\group\Plugin\GroupContentEnablerBase;
use Drupal\workbench_moderation\Entity\ModerationState;
use Drupal\workbench_moderation\Entity\ModerationStateTransition;


/**
 * Provides a content enabler for nodes.
 *
 * @GroupContentEnabler(
 *   id = "group_workbench",
 *   label = @Translation("Group Workbench"),
 *   description = @Translation("Adds nodes to groups both publicly and privately."),
 *   entity_type_id = "node",
 *   entity_access = TRUE,
 *   reference_label = @Translation("Title"),
 *   reference_description = @Translation("The title of the node to add to the group"),
 *   deriver = "Drupal\workbench_group\Plugin\GroupContentEnabler\GroupWorkBenchDeriver"
 * )
 */
class GroupWorkBench extends GroupContentEnablerBase {

  protected function getTargetEntityPermissions() {
    $permissions = [];
    $states = ModerationState::loadMultiple();
    foreach ($states as $id => $state) {
      $state_arg = ['%state_name' => $state->label()];
      $defaults = ['title_args' => $state_arg];
      $permissions['view ' . $id . ' state'] = [
          'title' => "View the %state_name state",
        ] + $defaults;
    }
    /*
    foreach (ModerationStateTransition::loadMultiple() as $id => $transition) {
      $transition_arg = ['%transition_name' => $transition->label()];
      $defaults = ['title_args' => $transition_arg];
      $permissions['use ' . $id . ' transition'] = [
        'title' => "Use the %transition_name transition",
      ] + $defaults;
    }*/

    //$permissions = parent::getTargetEntityPermissions();
    $plugin_id = $this->getPluginId();

    // Add a 'view unpublished' permission by re-using most of the 'view' one.
    //$original = $permissions["view1 $plugin_id entity"];
    /*
    $permissions["view1 unpublished $plugin_id entity"] = [
        'title' => str_replace('View ', 'View unpublished ', $original['title']),
      ] ;
*/
    return $permissions;
  }
}