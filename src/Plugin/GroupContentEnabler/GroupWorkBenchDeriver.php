<?php
/**
 * Created by PhpStorm.
 * User: mohamed
 * Date: 16/07/2017
 * Time: 00:07
 */

namespace Drupal\workbench_group\Plugin\GroupContentEnabler;
use Drupal\node\Entity\NodeType;
use Drupal\Component\Plugin\Derivative\DeriverBase;

class GroupWorkBenchDeriver extends DeriverBase {

  /**
   * {@inheritdoc}.
   */
  public function getDerivativeDefinitions($base_plugin_definition) {
   // foreach (NodeType::loadMultiple() as $name => $node_type) {
     // $label = $node_type->label();
      $this->derivatives['group_workbench'] = [
          //'entity_bundle' => $name,
          'label' => t('Group Workbench') /*. " ($label)"*/,
          'description' => t('Access content of groups both publicly and privately according to moderation state.'),
          /*'description' => t('Adds %type content to groups both publicly and privately.', ['%type' => $label]),*/
        ] + $base_plugin_definition;
    //}

    return $this->derivatives;
  }

}